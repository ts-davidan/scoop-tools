Scoop (http://scoop.sh/) is a command-line package manager similar to Chocolatey, but specializing in portable apps not requiring elevated or system-level installation.

An app 'installed' by scoop is extracted to your local `%USERPROFILE%/scoop/apps` directory, and executable shims are automatically added to your path under `%USERPROFILE%/scoop/shims`.

This repository is a custom Scoop [bucket](https://github.com/lukesampson/scoop/wiki/Buckets) for various tools that are not in the official Scoop repo.

1. Install Scoop (from powershell):

    > iex (new-object net.webclient).downloadstring('https://get.scoop.sh')

2. Add the bucket:

    > scoop bucket add tools https://bitbucket.org/ts-davidan/scoop-tools
    
3. Install tools:

    > eg.
    > scoop install StructuredLogViewer